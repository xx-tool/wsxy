package com.ly.service;

import com.ly.component.threadpool.httpclient.WSXYHttpClient;
import com.ly.model.entity.CourseDetailInfo;
import com.ly.model.entity.CourseInfo;
import com.ly.model.entity.ResponseEntity;
import com.ly.model.form.CourseForm;
import com.ly.utils.MyJsonUtil;
import com.ly.utils.MyObjectUtil;
import org.openqa.selenium.Cookie;

import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings("all")
public class CourseService {
	private String coursePath;
	private WSXYHttpClient httpClient;

	public CourseService(Set<Cookie> cookies, String coursePath) {
		httpClient = new WSXYHttpClient("wsxy.chinaunicom.cn", cookies);
		this.coursePath = coursePath;
	}

	public List<CourseInfo> page(int page, int size) {
		CourseForm courseForm = new CourseForm();
		courseForm.setPage(0);
		courseForm.setSize(1000);
		String res = httpClient.doGet(coursePath, courseForm);
		if (res != null) {
			ResponseEntity responseEntity = MyJsonUtil.string2Obj(res, ResponseEntity.class);
			if (responseEntity != null) {
				return responseEntity.getContent().parallelStream()
									 .filter(courseInfo -> courseInfo.getProgress() == null ||
														   courseInfo.getProgress() < 100)
									 .sorted((a, b) -> {
										 try {
											 Double aProgress =
													 1 - MyObjectUtil.getOrDefault(a.getProgress(), 0.0) / 100;
											 Double bProgress =
													 1 - MyObjectUtil.getOrDefault(b.getProgress(), 0.0) / 100;
											 Double aDuration = (a.getDuration() + 1) * aProgress;
											 Double bDuration = (b.getDuration() + 1) * bProgress;
											 if (a.getPeriod() == null || b.getPeriod() == null) {
												 if (aDuration - bDuration > 0) {
													 return 1;
												 } else if (aDuration - bDuration < 0) {
													 return -1;
												 } else {
													 return 0;
												 }
											 }
											 if (a.getPeriod() / aDuration - b.getPeriod() / bDuration > 0) {
												 return -1;
											 } else if (a.getPeriod() / aDuration - b.getPeriod() / bDuration < 0) {
												 return 1;
											 } else {
												 if (aDuration - bDuration > 0) {
													 return 1;
												 } else if (aDuration - bDuration < 0) {
													 return -1;
												 } else {
													 return 0;
												 }
											 }
										 } catch (Exception e) {
										 }
										 return 0;
									 })
									 .collect(Collectors.toList());
			}
		}
		return new ArrayList<>();
	}

	public CourseDetailInfo getCourseDetail(Integer id) {
		String path = String.format("http://wsxy.chinaunicom.cn/api/learner/offering-courses/%d", id);
		String res = httpClient.doGet(path, null);
		if (res != null) {
			return MyJsonUtil.string2Obj(res, CourseDetailInfo.class);
		}
		return null;
	}

	public boolean check(CourseDetailInfo courseDetailInfo) {
		String path = String.format("http://wsxy.chinaunicom.cn/api/learner/play/course/%d/check?classroomId=%d", courseDetailInfo
				.getCourseId(), courseDetailInfo.getOfferingId());
		String res = httpClient.doGet(path, null);
		if (res != null) {
			return Boolean.parseBoolean(res);
		}
		return false;
	}

	public String outline(CourseDetailInfo courseDetailInfo) {
		String path = String.format("http://wsxy.chinaunicom.cn/api/learner/play/course/%d/outline", courseDetailInfo.getCourseId());
		return httpClient.doGet(path, null);
	}

	public String offer(CourseDetailInfo courseDetailInfo) {
		String path = String.format("http://wsxy.chinaunicom.cn/api/learner/offering/%d/course/%d", courseDetailInfo.getOfferingId(), courseDetailInfo
				.getCourseId());
		return httpClient.doGet(path, null);
	}

	public String play(CourseDetailInfo courseDetailInfo) {
		//Referer: http://wsxy.chinaunicom.cn/learner/play/course/49165708;classroomId=49659088;courseDetailId=58449;learnerAttemptId=1621841908191
		String path = String.format("http://wsxy.chinaunicom.cn/learner/play/course/%d", courseDetailInfo.getCourseId());
		Map<String, Object> map = new HashMap<>();
		map.put("classroomId", courseDetailInfo.getOfferingId());
		map.put("courseDetailId", courseDetailInfo.getId());
		return httpClient.doGet(path, null);
	}

	public String save(CourseDetailInfo courseDetailInfo) {
		String path = String.format("http://wsxy.chinaunicom.cn/api/learner/play/course/%d/save", courseDetailInfo.getCourseId());
		return httpClient.doPost(path, null);
	}

	public void stop() {
		try {
			httpClient.close();
		} catch (Exception e) {

		}
	}
}
