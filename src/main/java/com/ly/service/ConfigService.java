package com.ly.service;

import com.ly.model.entity.ConfigEntity;
import com.ly.utils.MyJsonUtil;

import java.io.*;

public class ConfigService {
    public ConfigEntity configEntity;
    private File configFile = new File("config.json");

    public ConfigService() {
        if (!configFile.exists()) {
            configEntity = null;
        } else {
            read();
        }
    }

    public void read() {
        if (configFile.length() != 0) {
            try (FileInputStream fis = new FileInputStream(configFile)) {
                try (InputStreamReader isr = new InputStreamReader(fis)) {
                    try (BufferedReader br = new BufferedReader(isr)) {
                        String line;
                        StringBuilder stringBuilder = new StringBuilder();
                        while ((line = br.readLine()) != null) {
                            stringBuilder.append(line);
                        }
                        configEntity = MyJsonUtil.string2Obj(stringBuilder.toString(), ConfigEntity.class);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
