package com.ly.service;

import com.ly.model.entity.CourseInfo;

import java.util.Deque;

public class CourseQueueService {
    public Deque<CourseInfo> courseInfos;

    public CourseQueueService() {
    }

    public synchronized void setInfos(Deque<CourseInfo> courseInfos) {
        this.courseInfos = courseInfos;
    }

    public synchronized CourseInfo popCourseInfo() {
        try {
            return courseInfos.poll();
        } catch (Exception e) {
            return null;
        }
    }
}
