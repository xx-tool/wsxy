package com.ly;

import com.ly.service.EngineService;
import sun.misc.Signal;

public class Main {
    public static void main(String[] args) throws Exception {
        int threadNum = 1;
        if (args.length == 1) {
            threadNum = Integer.parseInt(args[0]);
        }
        System.out.println(String.format("启动%d个线程模拟", threadNum));
        EngineService engineService = new EngineService(threadNum);
        engineService.start();
        Runtime.getRuntime().addShutdownHook(new Thread(engineService::stop));
        Signal sg = new Signal("TERM");
        Signal.handle(sg, sig -> {
            System.out.println("处理15信号");
            System.exit(0);
        });
    }
}
