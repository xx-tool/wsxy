package com.ly.utils;

import java.io.File;
import java.net.URI;

public class MyUriUtil {

	public static URI toUri(String name) {
		File file = new File(name);
		if (file.exists()) {
			return file.toURI();
		} else {
			try {
				StringBuilder newUri = new StringBuilder();
				newUri.append("mfm:///");
				newUri.append(name.replace('.', '/'));
				String ext = ".java";
				if (name.endsWith(ext)) {
					newUri.replace(newUri.length() - ext.length(), newUri.length(), ext);
				}
				return URI.create(newUri.toString());
			} catch (Exception e) {
				return URI.create("mfm:///com/sun/script/java/java_source");
			}
		}
	}
}
