package com.ly.utils;

import java.util.List;
import java.util.Map;

@SuppressWarnings("all")
public class MyMapUtil {

	private MyMapUtil() {
	}

	public static <T> T get(Map<?, ?> map, String key) {
		try {
			return (T) map.get(key);
		} catch (Exception e) {
			return null;
		}
	}

	public static <T> T mapNode(Map<?, ?> map, String path) {
		try {
			String[] components = path.split("/");
			String component;
			Map<?, ?> temp = map;
			for (int i = 0; i < components.length; i++) {
				component = components[i];
				if (component.equals("")) {
					continue;
				}
				Object target;
				if (component.matches("^.+\\[[0-9]+]")) {
					String name = component.replaceAll("\\[[0-9]+]$", "");
					String indexPattern = component.replaceAll("(^.+\\[)|]$", "");
					int index = Integer.parseInt(indexPattern);
					List<?> list = (List<?>) temp.get(name);
					target = list.get(index);
				} else {
					target = temp.get(component);
				}
				if (i == components.length - 1) {
					return (T) target;
				}
				temp = (Map<?, ?>) target;
			}
			return (T) temp;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
