package com.ly.utils;

public class MyRandomUtil {

	public static double random(double start, double end) {
		double random = Math.random();
		random *= Math.abs(end - start);
		random += start;
		return random;
	}
}
