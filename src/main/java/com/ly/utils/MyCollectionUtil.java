package com.ly.utils;

import java.util.Collection;
import java.util.Map;

public class MyCollectionUtil {
	public static boolean isEmpty(Collection<?> collection) {
		return collection == null || collection.isEmpty();
	}

	public static boolean isEmpty(Map<?, ?> collection) {
		return collection == null || collection.isEmpty();
	}
}
