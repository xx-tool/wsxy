package com.ly.utils;

import java.util.stream.Stream;

public class MyBooleanUtil {
	private MyBooleanUtil() {}

	public static boolean isTrue(Boolean bool) {
		return Boolean.TRUE.equals(bool);
	}

	public static boolean isFalse(Boolean bool) {
		return !isTrue(bool);
	}

	public static boolean and(Boolean... arrays) {
		if (arrays.length == 0) {
			return false;
		}
		return Stream.of(arrays).parallel()
					 .noneMatch(MyBooleanUtil::isFalse);
	}

	public static boolean or(Boolean... arrays) {
		if (arrays.length == 0) {
			return false;
		}
		return Stream.of(arrays).parallel()
					 .anyMatch(MyBooleanUtil::isTrue);
	}
}
