package com.ly.utils;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.UUID;

public class MyStringUtil {

	private MyStringUtil() {
	}

	public static boolean isEmpty(String string) {
		return string == null || string.length() == 0;
	}

	public static boolean nonEmpty(String string) {
		return !isEmpty(string);
	}

	public static String randomID() {
		return UUID.randomUUID().toString().replace("-", "");
	}

	public static String randomString() {
		try {
			String token = randomID();
			Encoder encoder = Base64.getEncoder();
			MessageDigest md = MessageDigest.getInstance("md5");
			byte[] md5 = md.digest(token.getBytes());
			return encoder.encodeToString(md5);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return "";
		}
	}
}
