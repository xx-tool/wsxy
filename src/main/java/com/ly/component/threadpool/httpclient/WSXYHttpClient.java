package com.ly.component.threadpool.httpclient;

import com.fasterxml.jackson.core.type.TypeReference;
import com.ly.model.enums.RequestMethod;
import com.ly.utils.MyJsonUtil;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.util.EntityUtils;
import org.openqa.selenium.Cookie;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class WSXYHttpClient {
	private CloseableHttpClient httpClient;
	private String domain;

	public WSXYHttpClient(String domain, Set<Cookie> cookies) {
		this.domain = domain;
		CookieStore cookieStore = new BasicCookieStore();
		List<BasicClientCookie> basicClientCookies = cookies.parallelStream()
															.map(cookie -> {
																BasicClientCookie bc = new BasicClientCookie(cookie.getName(), cookie
																		.getValue());
																bc.setPath("/");
																bc.setDomain(this.domain);
																bc.setVersion(0);
																return bc;
															})
															.collect(Collectors.toList());
		for (BasicClientCookie basicClientCookie : basicClientCookies) {
			cookieStore.addCookie(basicClientCookie);
		}
		httpClient = HttpClientBuilder.create().setDefaultCookieStore(cookieStore).build();
	}

	public WSXYHttpClient() {
		httpClient = HttpClientBuilder.create().build();
	}

	public String doGet(String path, Object form) {
		return request(path, RequestMethod.GET, form);
	}

	public String doPost(String path, Object form) {
		return request(path, RequestMethod.POST, form);
	}

	public void close() {
		try {
			httpClient.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String request(String path, RequestMethod method, Object form) {
		try {
			URIBuilder uriBuilder = getWithParams(path, form);
			if (uriBuilder == null) {
				return null;
			}
			HttpUriRequest httpRequest = null;
			switch (method) {
				case GET:
					httpRequest = new HttpGet(uriBuilder.build());
					break;
				case POST:
					httpRequest = new HttpPost(uriBuilder.build());
					break;
			}
			try (CloseableHttpResponse response = httpClient.execute(httpRequest)) {
				if (response.getStatusLine().getStatusCode() == 200) {
					return EntityUtils.toString(response.getEntity(), "utf-8");
				}
			} catch (IOException e) {
			}
		} catch (URISyntaxException e) {
		}
		return null;
	}

	private URIBuilder getWithParams(String path, Object form) {
		try {
			URIBuilder uriBuilder = new URIBuilder(path);
			if (form == null) {
				return uriBuilder;
			}
			Map<String, Object> params = MyJsonUtil.obj2Obj(form, new TypeReference<Map<String, Object>>() {
			});
			if (params == null) {
				return uriBuilder;
			}
			for (Map.Entry<String, Object> entry : params.entrySet()) {
				String k = entry.getKey();
				Object v = entry.getValue();
				if (v != null) {
					uriBuilder.addParameter(k, v.toString());
				}
			}
			return uriBuilder;
		} catch (URISyntaxException e) {
			e.printStackTrace();
			return null;
		}
	}
}
