package com.ly.component.threadpool.callback;


import java.util.Map;

public interface SubmitRunnable {
	boolean run(Map<String, Object> params);
}
