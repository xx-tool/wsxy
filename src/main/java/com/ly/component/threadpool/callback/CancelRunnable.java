package com.ly.component.threadpool.callback;

import java.util.Map;

public interface CancelRunnable {
	boolean run(Map<String, Object> params);
}
