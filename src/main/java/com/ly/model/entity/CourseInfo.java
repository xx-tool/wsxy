package com.ly.model.entity;

import java.io.Serializable;

@SuppressWarnings("unused")
public class CourseInfo implements Serializable {


	private Long id;
	private String name;
	private Long sourceCourseId;
	private String description;
	private String code;
	private Integer duration;
	private Double period;
	private Double progress;
	private String version;
	private String status;
	private String imageUrl;
	private Long categoryId;
	private String categoryName;
	private String point;
	private Integer learners;
	private Integer likes;
	private String courseType;
	private String platform;
	private String platforms;
	private String rcoId;
	private Integer offeringCourseId;
	private String courseCollectionId;
	private Boolean isArchived;
	private Boolean isCollected;
	private Boolean isLiked;
	private String courseGroupId;
	private String groupName;

	public CourseInfo() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getSourceCourseId() {
		return sourceCourseId;
	}

	public void setSourceCourseId(Long sourceCourseId) {
		this.sourceCourseId = sourceCourseId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Double getPeriod() {
		return period;
	}

	public void setPeriod(Double period) {
		this.period = period;
	}

	public Double getProgress() {
		return progress;
	}

	public void setProgress(Double progress) {
		this.progress = progress;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getPoint() {
		return point;
	}

	public void setPoint(String point) {
		this.point = point;
	}

	public Integer getLearners() {
		return learners;
	}

	public void setLearners(Integer learners) {
		this.learners = learners;
	}

	public Integer getLikes() {
		return likes;
	}

	public void setLikes(Integer likes) {
		this.likes = likes;
	}

	public String getCourseType() {
		return courseType;
	}

	public void setCourseType(String courseType) {
		this.courseType = courseType;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getPlatforms() {
		return platforms;
	}

	public void setPlatforms(String platforms) {
		this.platforms = platforms;
	}

	public String getRcoId() {
		return rcoId;
	}

	public void setRcoId(String rcoId) {
		this.rcoId = rcoId;
	}

	public Integer getOfferingCourseId() {
		return offeringCourseId;
	}

	public void setOfferingCourseId(Integer offeringCourseId) {
		this.offeringCourseId = offeringCourseId;
	}

	public String getCourseCollectionId() {
		return courseCollectionId;
	}

	public void setCourseCollectionId(String courseCollectionId) {
		this.courseCollectionId = courseCollectionId;
	}

	public Boolean getIsArchived() {
		return isArchived;
	}

	public void setIsArchived(Boolean archived) {
		isArchived = archived;
	}

	public Boolean getIsCollected() {
		return isCollected;
	}

	public void setIsCollected(Boolean collected) {
		isCollected = collected;
	}

	public Boolean getIsLiked() {
		return isLiked;
	}

	public void setIsLiked(Boolean liked) {
		isLiked = liked;
	}

	public String getCourseGroupId() {
		return courseGroupId;
	}

	public void setCourseGroupId(String courseGroupId) {
		this.courseGroupId = courseGroupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
}

