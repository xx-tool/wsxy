package com.ly.model.entity;

import lombok.Data;

import java.util.List;

@SuppressWarnings("unused")
@Data
public class CourseDetailInfo {

	private String type;
	private Integer id;
	private Long offeringId;
	private Long courseId;
	private Long sourceCourseId;
	private Integer evaluateBoxId;
	private String noticeBoxId;
	private String userGroupId;
	private String name;
	private String offeringName;
	private String archivedStatus;
	private String courseName;
	private String keyWords;
	private String description;
	private String startDate;
	private String endDate;
	private String enrollStart;
	private String enrollEnd;
	private Boolean allowedEnroll;
	private String enrollStatus;
	private String enrollType;
	private String enrollMax;
	private String status;
	private String completedDate;
	private Boolean isEnd;
	private Integer likes;
	private Boolean isLiked;
	private Integer learners;
	private Integer collectors;
	private Double progress;
	private String imageUrl;
	private String courseType;
	private String code;
	private Double duration;
	private Double period;
	private String platform;
	private List<String> browsers;
	private Integer point;
	private String stage;
	private Boolean isCollected;
	private Boolean isPublished;
	private String excludeEnrolled;
	private String categoryName;
	private String categoryId;
	private String courseCollectionId;
	private String collectedDate;
	private String lastStudyDate;
	private String isArchived;
	private Boolean offeringArchived;
	private Integer score;
	private Integer totalTime;
	private Integer totalTimes;
	private Boolean salesEnabled;
	private String originalPrice;
	private String price;
	private String monetaryUnit;
	private String reviewTime;
	private String reviewTimes;
	private String avgScore;
	private String reviewName;
	private String associatedLabel;
	private String packageId;
	private String packageTitle;
	private Boolean recordedFlag;
}
