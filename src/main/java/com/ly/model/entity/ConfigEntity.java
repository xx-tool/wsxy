package com.ly.model.entity;

public class ConfigEntity {
	private String driver;
	private String bin;
	private String username;
	private String password;
	private String courseInfoPath;
	private String browser;
	private String captchaServer;

	public ConfigEntity() {
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public String getBin() {
		return bin;
	}

	public void setBin(String bin) {
		this.bin = bin;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public String getCaptchaServer() {
		return captchaServer;
	}

	public void setCaptchaServer(String captchaServer) {
		this.captchaServer = captchaServer;
	}

	public String getCourseInfoPath() {
		return courseInfoPath;
	}

	public void setCourseInfoPath(String courseInfoPath) {
		this.courseInfoPath = courseInfoPath;
	}
}
