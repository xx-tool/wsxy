package com.ly.model;

public class Tuple<A, B> {
	private A left;
	private B right;

	public Tuple(A left, B right) {
		this.left = left;
		this.right = right;
	}

	public Tuple() {
	}

	public static <A, B> Tuple<A, B> of(A left, B right) {
		return new Tuple<>(left, right);
	}

	public A getLeft() {
		return left;
	}

	public void setLeft(A left) {
		this.left = left;
	}

	public B getRight() {
		return right;
	}

	public void setRight(B right) {
		this.right = right;
	}
}
