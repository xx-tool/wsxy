package com.ly.model.form;

import lombok.Data;

@Data
public class CaptchaForm {
	private String img;
	private int compress = 0;
}
